/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <termios.h>
#include <sys/signal.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define STDIO_MODE_NORMAL		0 // built-in 32 byte buffers, circular recv
#define STDIO_MODE_BUFRECV		1 // supplied buffer, normal recv
#define SAVE_BLOCKSIZE 512

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
unsigned char copybuffer[SAVE_BLOCKSIZE+4];
unsigned int sum;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
uint32_t GetTick ()
{
	struct timeval tv;
	gettimeofday (&tv, 0);
	return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}

void Delay (uint32_t time)
{
	struct timespec req;

	req.tv_sec = time / 1000;
	req.tv_nsec = (time % 1000) * 1000000;

	nanosleep (&req, 0);
}

int ttysetup (int speed)
{
	// Wait
	Delay (100);

	// Set speed
	if (speed)
	{
		struct termios ty;
		tcgetattr (0, &ty);
		cfsetospeed (&ty, speed);
		tcsetattr (0, 0, &ty);
	}

	// Set mode
	if (ioctl (0, TTYSETMODE, (int) STDIO_MODE_BUFRECV))
		return 1;

	// Wait
	Delay (100);

	// Success
	return 0;
}

int tblock (int file, int nbytes, unsigned int blockno)
{
	int ret = 0;
	char txbuf[5];
	char *tx = "r";
	int txlen = 1;

restartblock: ;
	uint32_t start = GetTick ();

	while (read (0, copybuffer, (SAVE_BLOCKSIZE+4)) != (SAVE_BLOCKSIZE+4))
		if (GetTick () - start > 10000)
			return 1;

	while (write (1, tx, txlen) != txlen)
		if (GetTick () - start > 10000)
			return 2;

	while ((ret = ioctl (0, TTYGETRXBUSY, 0)) == 1)
		if (GetTick () - start > 10000)
			return 3;

	if (ret == 2)
	{
		txbuf[0] = 'e';
		txbuf[1] = blockno >> 0;
		txbuf[2] = blockno >> 8;
		txbuf[3] = blockno >> 16;
		txbuf[4] = blockno >> 24;
		tx = txbuf;
		txlen = 5;
		Delay (100);
		goto restartblock;
	}

	unsigned int lsum = 0;
	unsigned int check = ((unsigned int*)(copybuffer+SAVE_BLOCKSIZE))[0];

	for (int i = 0; i < SAVE_BLOCKSIZE/4; i++)
		lsum += ((unsigned int*)copybuffer)[i];

	if (lsum != check)
	{
		txbuf[0] = 'c';
		txbuf[1] = blockno >> 0;
		txbuf[2] = blockno >> 8;
		txbuf[3] = blockno >> 16;
		txbuf[4] = blockno >> 24;
		tx = txbuf;
		txlen = 5;
		Delay (100);
		goto restartblock;
	}
	else
		sum += lsum;

	if (write (file, copybuffer, nbytes) != nbytes)
		return 4;
	return 0;
}

int save (int file, int nbytes)
{
	// Calculate blocks and remainder
	int nblocks = nbytes / SAVE_BLOCKSIZE;
	int nremain = nbytes % SAVE_BLOCKSIZE;

	// Return code for write calls
	int ret = 0;

	// For each block
	for (int i = 0; i < nblocks; i++)
		if ((ret = tblock (file, SAVE_BLOCKSIZE, i)))
			return ret;

	// If there is a remainder
	if (nremain)
		ret = tblock (file, nremain, 0xFFFFFFFF);

	// Indicate complete
	uint32_t start = GetTick ();
	while (write (1, "o", 1) != 1)
		if (GetTick () - start > 10000)
			return 9;

	// Make sure it is sent
	Delay (10);

	// Success
	return ret ? 4 + ret : 0;
}

int main (int argc, char **argv)
{
	// Usage
	if (argc != 4 && argc != 3)
	{
		SimplePrint ("Usage:\n");
		SimplePrint ("save <path> <nbytes> <baud>\n");
		SimplePrint ("save <path> <nbytes>\n");
		return 1;
	}

	// Get number of bytes and speed
	int nbytes = atoi (argv[2]);
	int speed = 0;

	if (argc == 4)
		speed = atoi (argv[3]);

	// Input check
	if (nbytes <= 0)
	{
		SimplePrint ("Number of bytes <= 0\n");
		return 2;
	}

	if (speed && (speed < 300 || speed > 10000000))
	{
		SimplePrint ("Speed < 300 or > 10M\n");
		return 3;
	}

	// Open target
	int target = open (argv[1], O_WRONLY | O_TRUNC | O_CREAT);

	// Check file open
	if (target < 0)
	{
		SimplePrint ("Could not open ");
		SimplePrint (argv[2]);
		SimplePrint ("\n");
		return 4;
	}

	// Set speed and tty mode
	if (ttysetup (speed))
	{
		ioctl (0, TTYSETMODE, (int) STDIO_MODE_NORMAL);
		SimplePrint ("Failed to set tty mode\n");
		close (target);
		return 5;
	}

	// Save
	sum = 0;
	int ret = save (target, nbytes);

	// Reset tty
	ioctl (0, TTYSETMODE, (int) STDIO_MODE_NORMAL);

	// Close file
	close (target);

	// Check
	if (ret)
	{
		switch (ret)
		{
		case 1: SimplePrint ("block rd err\n"); break;
		case 2: SimplePrint ("block wr r err\n"); break;
		case 3: SimplePrint ("block ioctl err\n"); break;
		case 4: SimplePrint ("block wr f err\n"); break;
		case 5: SimplePrint ("rem rd err\n"); break;
		case 6: SimplePrint ("rem wr r err\n"); break;
		case 7: SimplePrint ("rem ioctl err\n"); break;
		case 8: SimplePrint ("rem wr f err\n"); break;
		default: SimplePrint ("unknown err\n");
		}

		return 6 + ret;
	}

	// Print sum
	char str[10];
	itoa_uint32_hex (sum, str);
	SimplePrint (str);
	SimplePrint ("\n");

	// Success
	return 0;
}


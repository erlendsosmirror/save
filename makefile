###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = save
PROJECT_FILES = $(shell find src -name "*.*")

USR_INC = -I../../lib/libuser/inc -I./inc
USR_LIB = -L../../lib/libuser/build
USR_LIBS = -luser

###############################################################################
# Compiler flags, file processing and makefile execution
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets
